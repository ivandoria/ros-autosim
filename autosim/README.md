# ROS Autosim

This project goal is to provide a Python scripting interface to generate and execute custom launch files based on some input parameters.

## Installation

Clone this repository in the src folder of your workspace and run catkin_make.

## Execution

First, adjust the path to the autosim folder and input file name in `src/launchLMAS.py`.

To run the autosim, enter in a new terminal:

```bash
roslaunch autosim launchLMAS.launch
```

## How it works

The command above executes `launchLMAS.py`. This file looks for the input file in the `autosim/input` folder and creates a simple service server and a client for each description contained in the input file.
