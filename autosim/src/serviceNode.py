#!/usr/bin/env python2

import rospy
from autosim.srv import simpleService, simpleServiceRequest, simpleServiceResponse


class ServiceNode():

    def __init__(self):
        self._id = ""
        self._status = False
        rospy.init_node("simpleServiceServer", anonymous=True)

    def handleSimpleService(self, req):
        rospy.loginfo("simpleServiceHandler!")
        self._status = not(self._status)
        rospy.loginfo("New status: " + str(self._status))
        return simpleServiceResponse(self._status)

    def simpleServiceServer(self):
        s = rospy.Service("simpleService", simpleService, self.handleSimpleService)
        rospy.loginfo("Ready to execute service.")
        rospy.spin()


if __name__ == "__main__":
    node = ServiceNode()
    node.simpleServiceServer()
