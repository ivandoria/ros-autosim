#!/usr/bin/env python2

import os
import sys
import csv


class fileGenerator():

    _pathToRoot = "/home/ivan/catkin_autosim/src/ros-autosim/autosim/"

    _inputListOfDicts = []

    _header = '<?xml version="1.0"?>\n'

    _tagLaunch = '<launch>\n'
    _tagCLaunch = '</launch>'

    _tagNode1Prefix = '  <arg name="node1Prefix" default="node1"/>\n'
    _tagNode1Base = '  <arg name="node1Base" default="100"/>\n'
    _tagNode1Rate = '  <arg name="node1Rate" default="1"/>\n'

    _tagNode2Prefix = '  <arg name="node2Prefix" default="node2"/>\n'
    _tagNode2Base = '  <arg name="node2Base" default="500"/>\n'
    _tagNode2Rate = '  <arg name="node2Rate" default="0.5"/>\n'

    _tagNode1Server = '  <node name="oddEvenNode1" pkg="autosim" type="oddEvenNode.py" output="screen" args="$(arg node1Prefix)" />\n'
    _tagNode1Client = '  <node name="oddEvenClient1" pkg="autosim" type="oddEvenClient.py" output="screen" args="$(arg node1Prefix) $(arg node1Base) $(arg node1Rate)" />\n'

    _tagNode2Server = '  <node name="oddEvenNode2" pkg="autosim" type="oddEvenNode.py" output="screen" args="$(arg node2Prefix)" />\n'
    _tagNode2Client = '  <node name="oddEvenClient2" pkg="autosim" type="oddEvenClient.py" output="screen" args="$(arg node2Prefix) $(arg node2Base) $(arg node2Rate)" />\n'

    _tagBreakLine = '\n'

    # def __init__(self):
    def writeToFile(self, fileName):
        os.chdir(self._pathToRoot + "launch")
        f = open(fileName, "w")
        f.write(self._header)
        f.write(self._tagLaunch)
        f.write(self._tagBreakLine)

        f.write(self._tagNode1Prefix)
        f.write(self._tagNode1Base)
        f.write(self._tagNode1Rate)
        f.write(self._tagBreakLine)

        f.write(self._tagNode2Prefix)
        f.write(self._tagNode2Base)
        f.write(self._tagNode2Rate)
        f.write(self._tagBreakLine)

        f.write(self._tagNode1Server)
        f.write(self._tagNode1Client)
        f.write(self._tagBreakLine)

        f.write(self._tagNode2Server)
        f.write(self._tagNode2Client)
        f.write(self._tagBreakLine)

        f.write(self._tagCLaunch)
        f.close()

    def readFile(self, fileName):
        f = open(fileName, "r")
        print(f.read())

    def dirTest(self):
        os.chdir(self._pathToRoot + "launch")
        filename = "demo1.launch"
        f = open(filename, "w")
        f.write(self._header)
        f.close()
        self.readFile(filename)

    def loadInput(self, a_csvFilename):
        # Change directory to input folder
        os.chdir(self._pathToRoot + "input")

        with open(a_csvFilename) as f:
            reader = csv.reader(f)
            self._inputListOfKeys = reader.next()

        with open(a_csvFilename) as f:
            reader = csv.DictReader(f)
            for line in reader:
                self._inputListOfDicts.append(line)
        return

    def printInputData(self):
        print("\n--- Start of input data:")
        for i in self._inputListOfDicts:
            print("---")
            for j in self._inputListOfKeys:
                print(str(j) + ": " + str(i[j]))
        print("---\n--- End of input data.\n")
        return

    def checkPath(self):
        try:
            os.chdir(self._pathToRoot)
        except OSError:
            print("Invalid path: " + self._pathToRoot)
            print("Exiting launchFileGenerator...")
            sys.exit()

    def main(self):
        self.checkPath()
        gen.loadInput("testCase1.csv")
        filename = "demo.launch"
        print("Generating launch files.")
        self.writeToFile(filename)
        print("Finished generating launch files.")
        print("Reading generated launch file:")
        self.readFile(filename)
        print("Finished reading generated launch file.\n")
        print("Execution:")
        # os.system("roslaunch autosim demo.launch")
        os.system("roslaunch assembly_simulation kairosSAS.launch")


if __name__ == "__main__":
    gen = fileGenerator()
    gen.main()
