#!/usr/bin/env python2

import sys

import rospy
from autosim.srv import simpleService, simpleServiceRequest, simpleServiceResponse


class ServiceClient():

    def __init__(self):
        self._id = ""

        rospy.init_node("serviceClient", anonymous=True)
        self._rate = rospy.Rate(1)

    def simpleServiceClient(self):
        rospy.wait_for_service('simpleService')
        while(not(rospy.is_shutdown())):
            try:
                service = rospy.ServiceProxy("simpleService", simpleService)
                resp1 = service().answer
                rospy.loginfo("Answer: " + str(resp1))
            except rospy.ServiceException as e:
                rospy.loginfo("Service call failed: %s" % e)
            self._rate.sleep()


if __name__ == "__main__":
    client = ServiceClient()
    client.simpleServiceClient()
