#!/usr/bin/env python2

import os
import sys
import csv
import time
import rospy
import roslaunch

pathToRoot = "/home/ivan/catkin_autosim/src/ros-autosim/autosim/"
nameOfInputFile = "testCase1.csv"


class fileGenerator():

    _inputListOfDicts = []

    def __init__(self, path, inputFilename):
        rospy.init_node("brainLMAS", anonymous=True)
        self._pathToRoot = path
        self._inputFilename = inputFilename

    def loadInput(self, a_csvFilename):
        # Change directory to input folder
        os.chdir(self._pathToRoot + "input")

        with open(a_csvFilename) as f:
            reader = csv.reader(f)
            self._inputListOfKeys = reader.next()

        with open(a_csvFilename) as f:
            reader = csv.DictReader(f)
            for line in reader:
                self._inputListOfDicts.append(line)
        return

    def printInputData(self):
        rospy.loginfo("--- Start of input data:")
        for i in self._inputListOfDicts:
            rospy.loginfo("---")
            for j in self._inputListOfKeys:
                rospy.loginfo(str(j) + ": " + str(i[j]))
        rospy.loginfo("--- End of input data.\n")
        return

    def checkPath(self):
        try:
            os.chdir(self._pathToRoot)
            rospy.loginfo("Loading files from path: " + self._pathToRoot)
        except OSError:
            rospy.loginfo("Invalid path: " + self._pathToRoot)
            rospy.loginfo("Exiting launchFileGenerator...")
            sys.exit()

    def launchServices(self):
        # Launch Servers
        for node in self._inputListOfDicts:
            uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(uuid)
            l_cli_args = [self._pathToRoot + '/launch/server.launch',
                          'prefix:=' + node["Prefix"]]
            l_roslaunch_args = l_cli_args[1:]
            l_roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(l_cli_args)[0], l_roslaunch_args)]
            parent = roslaunch.parent.ROSLaunchParent(uuid, l_roslaunch_file)
            parent.start()

        # Launch Clients
        for node in self._inputListOfDicts:
            uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(uuid)
            l_cli_args = [self._pathToRoot + '/launch/client.launch',
                          'prefix:=' + node["Prefix"], 'base:=' + node["Base"], 'rate:=' + node["Rate"]]
            l_roslaunch_args = l_cli_args[1:]
            l_roslaunch_file = [(roslaunch.rlutil.resolve_launch_arguments(l_cli_args)[0], l_roslaunch_args)]
            parent = roslaunch.parent.ROSLaunchParent(uuid, l_roslaunch_file)
            parent.start()

    def main(self):
        self.checkPath()
        self.loadInput(self._inputFilename)
        self.printInputData()
        self.launchServices()

        # Keeps nodes alive
        rospy.spin()


if __name__ == "__main__":
    gen = fileGenerator(pathToRoot, nameOfInputFile)
    gen.main()
