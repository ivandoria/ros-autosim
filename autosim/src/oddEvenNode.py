#!/usr/bin/env python2

import sys

import rospy
from autosim.srv import (oddEvenService, oddEvenServiceRequest,
                         oddEvenServiceResponse)


class ServiceNode():

    def __init__(self, prefix):
        self._prefix = prefix

    def handleOddEvenService(self, req):
        self._number = req.number
        rospy.loginfo(self._prefix + " Server-> Number received: " + str(self._number))

        if(self._number % 2 == 0):
            self._oddEven = "Even"
        else:
            self._oddEven = "Odd"

        rospy.loginfo(self._prefix + " Server -> Odd or Even: " + str(self._oddEven))
        return oddEvenServiceResponse(self._oddEven)

    def oddEvenServiceServer(self):
        s = rospy.Service(self._prefix + "/oddEvenService", oddEvenService, self.handleOddEvenService)
        rospy.loginfo(self._prefix + " is ready to execute service.")
        rospy.spin()


if __name__ == "__main__":
    myArgv = rospy.myargv(argv=sys.argv)
    prefix = myArgv[1]
    rospy.init_node(prefix + "oddEvenServiceServer", anonymous=False)
    rospy.loginfo(myArgv)
    node = ServiceNode(prefix)
    node.oddEvenServiceServer()
