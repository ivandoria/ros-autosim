#!/usr/bin/env python2

import sys

import rospy
from autosim.srv import (oddEvenService, oddEvenServiceRequest,
                         oddEvenServiceResponse)


class ServiceClient():

    def __init__(self, prefix, base, rate):
        self._prefix = prefix
        self._cont = base
        self._rate = rospy.Rate(rate)

    def oddEvenServiceClient(self):
        rospy.wait_for_service(self._prefix + "/oddEvenService")
        while(not(rospy.is_shutdown())):
            try:
                service = rospy.ServiceProxy(self._prefix + "/oddEvenService", oddEvenService)
                rospy.loginfo(self._prefix + " Client -> Number requested: " + str(self._cont))
                resp1 = service(self._cont).oddEven
                rospy.loginfo(self._prefix + " Client -> " + str(self._cont) + " is: " + str(resp1))
            except rospy.ServiceException as e:
                rospy.loginfo(self._prefix + ": Service call failed: %s" % e)
            self._cont = self._cont + 1
            rospy.loginfo("-----")
            self._rate.sleep()


if __name__ == "__main__":  # TODO: Initialize node with prefix
    myArgv = rospy.myargv(argv=sys.argv)
    prefix = myArgv[1]
    rospy.init_node(prefix + "_oddEvenServiceClient", anonymous=False)
    rospy.loginfo("Prefix: " + prefix)
    base = myArgv[2]
    rospy.loginfo("Base: " + str(base))
    rate = myArgv[3]
    rospy.loginfo("Rate: " + str(rate))
    client = ServiceClient(prefix, float(base), float(rate))
    client.oddEvenServiceClient()
